FROM caddy:builder AS builder

RUN apk add gcc libc-dev bash pcre-dev wget git

#
# CADDY SETUP
#
WORKDIR /usr/src/

RUN xcaddy build \
    --with github.com/corazawaf/coraza-caddy \
    --with github.com/caddyserver/replace-response \
    --with github.com/greenpau/caddy-security \
    --with github.com/kirsch33/realip \
    --with github.com/mastercactapus/caddy2-proxyprotocol \
    --with github.com/RussellLuo/caddy-ext/ratelimit \
    --with github.com/porech/caddy-maxmind-geolocation \
    --output /usr/bin/caddy

FROM caddy:2.4.6

COPY --from=builder /usr/bin/caddy  /usr/bin/caddy

RUN apk add pcre nss-tools git wget

WORKDIR /coraza

RUN git clone https://github.com/coreruleset/coreruleset.git
RUN mv /coraza/coreruleset/crs-setup.conf.example  /coraza/coreruleset/crs-setup.conf
RUN wget -c https://raw.githubusercontent.com/corazawaf/coraza/v2/master/coraza.conf-recommended -O ./coraza.conf
RUN rm -f /coraza/coreruleset/rules/REQUEST-922-MULTIPART-ATTACK.conf

COPY ./Caddyfile /coraza/

RUN apk del git wget

RUN rm -rf /tmp/*
CMD ["/usr/bin/caddy", "run", "--config", "/coraza/Caddyfile", "--adapter", "caddyfile", "--watch"]
